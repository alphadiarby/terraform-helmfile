resource "kubernetes_secret_v1" "docker_cfg" {
  metadata {
    name = "docker-cfg"
  }

  data = {
    ".dockerconfigjson" = jsonencode({
      auths = {
       
          auth = "${base64encode("${var.registry_username}:${var.registry_password}")}"
        
      }
    })
  }

  type = "kubernetes.io/dockerconfigjson"
}
# attention dans le cadre d'un déploiement ses données ci-dessous sont sensibles faudra crée un fichier tfvars 
resource "kubernetes_secret" "secret_mysql" {
  metadata {
    name = "secret-mysql"
    namespace = "back"
  }

  data = {
    MYSQL_DATABASE = "alpha"
    MYSQL_PASSWORD = "xxx"
    MYSQL_ROOT_PASSWORD = "alpha"
    MYSQL_USER = "test"
  }


}

resource "kubernetes_config_map" "my-config" {
  metadata {
    name = "db-sql"
    namespace = "back"
  }

  data = {
    "db.sql" = "${file("${path.module}/db.sql")}"
  }

}

# resource "kubernetes_config_map" "my-config1" {
#   metadata {
#     name = "dbconect"
#     namespace = "front"
#   }

#   data = {
#     DATABASE_URL = "mysql://test:xxx@mysql.back:3306/alpha"
#   }

# }