resource "kubernetes_persistent_volume" "front_persistent" {
  metadata {
    name = "front-persistent"
  }
  spec {
    capacity = {
      storage = "2Gi"
    }
    access_modes = ["ReadWriteOnce"]
    storage_class_name = "my-storage-class"
    persistent_volume_source {
      host_path  {
        path = "/data/html"
      }
    }
  }
}